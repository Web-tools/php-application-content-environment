<?php

define('PACE_DIR', dirname(__FILE__) . '/');

defined('DEBUG')    or define('DEBUG', 	false);
defined('SSL_ONLY') or define('SSL_ONLY', true);
defined('HONEYPOT') or define('HONEYPOT', 'STRICT');

require_once(PACE_DIR . 'debug.php');
require_once(PACE_DIR . 'general.php');

require_once(PACE_DIR . 'bouncer.php');
if ( SSL_ONLY != false ) require_once(PACE_DIR . 'ssl_only.php');

