<?php

function fileext($filename) {
    $p=pathinfo($filename);
	if ( isset($p['extension']) ) {
	    $ext=strtolower($p['extension']);
	} else {
		$ext='';
	}
    # if ( in_array($ext, array("gz", "bz2")) ) {
    #     $e=fileext(substr($filename, 0, -(strlen($ext) + 1)));
    #     if ( $e == "tar" ) $ext=$e . '.' . $ext;
    # }
    return $ext;
}

function filename($filename) {
    $p=pathinfo($filename);
    $name=$p['filename'];
    return $name;
}

function filepath($filename) {
    $p=pathinfo($filename);
    $name=$p['dirname'];
    return $name;
}
