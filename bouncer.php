<?php

# relies on apache rewriting uri of non-existing resources through index.php.

# Writes invalid requests of some paths & pages and all php requests to the error log file.
# So, the can be banned by fail2ban.

# Automatic honeypot has three modes... HONEYPOT=
# false  = disabled
# true   = all specified documents, specified queries and ANY *.php request that is not explicitly permitted is banned.
# STRICT = (current default), only specifed queries and documents requests are banned.

function bounce_client($message='') {

	error_log('PACE Honeypot error: ' . $message,0);
	# error_log('PACE saftey error: ' . $message,0);

	echo '<html><head>';
	echo '<title>Naughty or nice</title>';
	echo '</head>';
	echo '<body bgcolor="202020">';
	echo '<font color="white"><h2>Santa Claus is very disappointed in you. '
		. '</h2><h3>Instead of what you requested, it looks like you will be getting a lump of coal for Christmas.</h3></font>';
	echo '</body>';
	echo '</html>';
	die();

}

function honeypot() {

	$allow=array(
		"?"=>array('css=1', 'js=1'),
		"php"=>array('index'),
		"xml"=>array('sitemap'),
		"htm"=>array('index'),
		"html"=>array('index'),
		"css"=>array('index', 'welcome'),
		"txt"=>array(
			'ads', 				# https://en.wikipedia.org/wiki/Ads.txt
			'robots',			# https://en.wikipedia.org/wiki/Robots_exclusion_standard
			'security',			# https://en.wikipedia.org/wiki/Security.txt
			'humans'				# see alternatives to robots.txt
		),
		"ico"=>array('favicon', 'favicon-32x32', 'favicon-48x48'),
		"png"=>array('apple-touch-icon', 'apple-touch-icon-precomposed')
	);

	$honey=array(
		""=>array("phpmyadmin", "shell", "system", "joomla", "webdav"),
		"?"=>array("wt=json", "busybox", "s=captcha"),
		"cgi"=>array("setup", "mainfunction"),
		"php"=>array("eval-stdin", "modules", "wp-config", "wp-admin", "wp-login", "phpinfo", "xmlrpc",
		    "setup-config", "install"),
		"xml"=>array("wlwmanifest")
	);

	$uri=$_SERVER["REQUEST_URI"];
	$client=$_SERVER["REMOTE_ADDR"];
	$query='';
	$name='';
	$ext='';

	if ( $uri == '/index.php' ) {
		$redir = 'https://' . $_SERVER['HTTP_HOST'];
		header('Location: ' . $redir );
		echo '<html><head>';
		echo '<meta http-equiv="refresh" content="0;url=' . $redir . '">';
		echo '</html>';
		die();
	};

	if ( strpos($uri, '?') ) {
		$query=substr($uri, strpos($uri, '?') + 1);
		$uri=substr($uri, 0, strpos($uri, '?'));
	}

    $p=pathinfo($uri);
	if ( isset($p['extension']) ) {
	    $ext=strtolower($p['extension']);
	} else {
		$ext='';
	}
	if ( isset($p['filename']) ) {
	    $name=strtolower($p['filename']);
	} else {
		$name='';
	}

	# filter out allowed requests
	if ( array_key_exists($ext, $allow) && in_array($name, $allow["$ext"], true) ) {
		$name='';
		if (( HONEYPOT == 'STRICT' ) or ($ext != 'php')) {
			$ext='';
		}
	}
	if ( array_key_exists('?', $allow) && in_array($query, $allow["?"], true) ) {
		$query='';
	}

	if ( $name . $ext . $query == '' ) return;

	if (array_key_exists($ext, $honey) && in_array($name, $honey["$ext"], true)) {
		bounce_client ('forbidden request - ' . $name . ' - ' . $_SERVER["REQUEST_URI"]);
	}
	if (($query != "") && array_key_exists('?', $honey) && in_array($query, $honey['?'], true)) {
		bounce_client ('forbidden query - ' . $query . ' - ' . $_SERVER["REQUEST_URI"]);
	}

	if ($ext != "") {
		if (($ext == "php") && ( HONEYPOT != 'STRICT' )) {
			bounce_client ('forbidden all php - ' . $_SERVER["REQUEST_URI"]);
		} else {
			error_log('PACE saftey check: (' . $name . ($ext == '' ? '' : '.' . $ext) . ($query != '' ? ', query:' . $query : '') . ') ' . $_SERVER["REQUEST_URI"],0);
		}
	}

}

if ( HONEYPOT != 'FALSE' ) honeypot();