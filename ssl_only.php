<?php

function redirect_ssl () {
	if ( ! isset ( $_SERVER["HTTPS"] ) ) {
		$redir = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER["REQUEST_URI"];
		header('Location: ' . $redir );
		echo '<html><head>';
		echo '<meta http-equiv="refresh" content="0;url=' . $redir . '">';
		echo '</html>';
		die();
	}
}

redirect_ssl();